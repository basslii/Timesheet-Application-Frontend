import { Component, OnInit, } from '@angular/core';
import { firstValueFrom } from 'rxjs';
import { Page, Status, User } from 'src/app/interfaces/interfaces';
import { StatusService } from 'src/app/services/status-service.service';
import { UserService } from 'src/app/services/user-service.service';

@Component({
  selector: 'app-mainpage',
  templateUrl: './mainpage.component.html',
  styleUrls: ['./mainpage.component.css']
})
export class MainpageComponent implements OnInit {
  allUsers: User[] = [];
  allStatus: Status[] = [];
  searchModel: string = '';
  currentPage: number = 1;
  constructor(
    private userService: UserService,
    private statusService: StatusService,
  ){

  }

  async ngOnInit(): Promise<void> {
    this.allUsers = await firstValueFrom(this.userService.getAllUsers());
    this.allStatus = await firstValueFrom(this.statusService.getALLStatuses());
  }

  receiveDataFromSearchComp(event: string) {
    this.searchModel = event
  }
}
