import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output, } from '@angular/core';
import { MatDialog } from "@angular/material/dialog";
import { CreateNewTaskDialogComponent } from '../create-new-task-dialog/create-new-task-dialog.component';
import { NewTaskData, Page, Status, TimesheetData, User } from 'src/app/interfaces/interfaces';
import { InfoService } from 'src/app/services/info-service.service';
import { TimesheetService } from 'src/app/services/timesheet.service';
import * as moment from 'moment';
import { UserService } from 'src/app/services/user-service.service';
import { StatusService } from 'src/app/services/status-service.service';
import { firstValueFrom } from 'rxjs';

@Component({
  selector: 'app-search-input',
  templateUrl: './search-input.component.html',
  styleUrls: ['./search-input.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SearchInputComponent {
  @Input() allStatus: Status[] = [];
  @Input() allUsers: User[] = [];
  @Output() customEvent = new EventEmitter<string>();
  searchModel: string = '';
  
  constructor(
    private dialog: MatDialog,
    private infoService: InfoService,
    private timeSheetService: TimesheetService,
    private userService: UserService,
    private statusService: StatusService,
  ){}

  async loadPage(keyword: string = ''): Promise<void> {
    this.customEvent.emit(keyword);
  }
  
  async onSearchModel(): Promise<void> {
    (await this.timeSheetService.getDataBySearchTask(this.searchModel)).subscribe(async foundResult => {
      if (foundResult!.empty === false) {
        await this.loadPage(this.searchModel);
        this.infoService.showInfo('Successfully found result');
      } else {
        this.infoService.showErrorString(`Failed to find with keyword ${this.searchModel}`);
      }
    })

  }

  async onCreateNewModel(): Promise<void> {
    const data: Partial<NewTaskData> = {
      title: 'Timesheet Entry',
      fields: { Project: '', Task: this.searchModel, startDate: '', endDate: '', Status: '', AssignTo: '' },
      multiple: [false, false, false, false, true, true],
      date: [false, false, true, true, false, false],
      users: this.allUsers,
      status: this.allStatus,
      edit: false,
    }

    const dialogResponse = await firstValueFrom(this.dialog.open(CreateNewTaskDialogComponent, { disableClose: true, height: '90%', width: '50%', data}).afterClosed());

    if (dialogResponse) {

      console.log((this.allStatus.find(status => status.status === dialogResponse.fields.status))?.id, "id")
      const timesheetData: Partial<TimesheetData> = {
        project: dialogResponse.fields.Project,
        task: dialogResponse.fields.Task,
        assignedTo: await firstValueFrom(this.userService.getUserById(this.allUsers.find(user => user.name === dialogResponse.fields!.AssignTo!)!.id!.toString())),
        startDate: moment(dialogResponse.fields.startDate).format('DD-MM-YYYY'),
        endDate: moment(dialogResponse.fields.endDate).format('DD-MM-YYYY'),
        status: await firstValueFrom(this.statusService.getStatusById(this.allStatus.find(status => status.status === dialogResponse.fields!.Status!)!.id!.toString())),
      }
      this.searchModel = ''

      this.timeSheetService.createNewTask(timesheetData).subscribe(async createdTask => {
        if (createdTask) {
          await this.loadPage();
          this.infoService.showInfo('Successfully added new data');
        } else {
          this.infoService.showErrorString('Failed to add new data');
        }
      });
    }
  }

  onChangeKeyword(keyword: string) {
    if(keyword === '') {
      this.loadPage();
    }
  }

}
