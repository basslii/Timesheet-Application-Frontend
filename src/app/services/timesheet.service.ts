import { Injectable } from '@angular/core';
import { Page, TimeSheetReqBody, TimesheetData } from '../interfaces/interfaces';
import { Observable, firstValueFrom, from, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';
import { ApiService } from './api.service';
import * as moment from 'moment';

@Injectable({
  providedIn: 'root'
})
export class TimesheetService extends ApiService {

  async getDataBySearchTask(keyword: string = ''): Promise<Observable<Page>> {

    const uri = `${this.base}/tasks?keyword=${keyword}`;

    return this.http.get<Page>(uri).pipe(
      map((returnedData: Page) => {
        return {...returnedData, content: returnedData.content.map((data: TimesheetData) => this.convertToNormalBody(data))};
      })
    );
  }

  getTotalTasks(): Observable<number> {
    const uri = `${this.base}/tasks/count`;
    return this.http.get<number>(uri);
  }

  createNewTask(data: Partial<TimesheetData>): Observable<TimesheetData> {
    const uri = `${this.base}/tasks/add`;
    const reqBody = this.convertToReqBody(data);

    return this.http.post<TimesheetData>(uri, reqBody).pipe(
      map((returnedData: TimesheetData) => this.convertToNormalBody(returnedData))
    );
  }

  updateTask(data: TimesheetData, taskId: number): Observable<boolean> {
      const uri = `${this.base}/tasks/${taskId}`;
      return this.http.put<boolean>(uri, data);
  }

  deleteData(taskId: number): Observable<boolean> {
    const uri = `${this.base}/tasks/${taskId}`;
    return this.http.delete<boolean>(uri);
  }

  private convertToReqBody(data: Partial<TimesheetData>): Partial<TimesheetData> {
    const { startDate, endDate, ...otherElements} = data

    const convertedStartDate = moment(startDate, "DD-MM-YYYY").format("YYYY-MM-DDTHH:mm:ss")
    const convertedEndDate = moment(endDate, "DD-MM-YYYY").format("YYYY-MM-DDTHH:mm:ss")

    return { ...otherElements, startDate: convertedStartDate, endDate: convertedEndDate }
  }

  private convertToNormalBody(data: TimesheetData): TimesheetData {
    const { startDate, endDate, ...otherElements} = data

    const startDateString = startDate!.split('T')[0].split('-').reverse().join('-');
    const endDateString = endDate!.split('T')[0].split('-').reverse().join('-');

    return { ...otherElements, startDate: startDateString, endDate: endDateString }
  }
}
